import Promise from 'bluebird';
import crypto from 'crypto';
import httpStatus from 'http-status';
import mongoose from 'mongoose';
import mongooseFloat from 'mongoose-float';

import APIError from '../helpers/APIError';

import errorService from '../services/error.service';

const Float = mongooseFloat.loadType(mongoose);
const Schema = mongoose.Schema;

/**
 * Player Schema
 */
const PlayerSchema = new mongoose.Schema({
    fields: Object,
    rank: Number,
    firstName: String,
    lastName: String,
    displayName: String,
    regNumber: String,
    dob: Date,
    age: Number,
    state: String,
    eventDate: Date,
    category: String,
    bestEightSinglePoints: String,
    bestEightDoublePoints: String,
    twentyFivePercentBestEightDoublePoints: String,
    bestSixSinglePoints: String,
    bestSixDoublePoints: String,
    twentyFivePercentBestSixSinglePoints: String,
    pointsCutForNoShowLateWL: String,
    ttlFinalPoints: String,
    ttlPoints: String,
    itfPoints: String,
    twentyFivePercentPointsAsianU14: String,
    pointsUnder14: String,
    pointsUnder16: String,
    pointsUnder18: String,
    pointsUnderMens: String,
    pointsUnderWomens: String,
    createdByName: String,
    bestEightCategory: { type: Boolean, default: false },
    bestSixCategory: { type: Boolean, default: false },
    updated: Date,
    created: { type: Date, default: Date.now },
    status: { type: String, default: 'Active' },
    active: { type: Boolean, default: true },
    createdBy: {
        employee: { type: Schema.ObjectId, ref: 'Employee' },
        player: { type: Schema.ObjectId, ref: "Player" }
    },
    updatedBy: {
        employee: { type: Schema.ObjectId, ref: 'Employee' },
        player: { type: Schema.ObjectId, ref: "Player" }
    },

}, { usePushEach: true });


/**
 * Hook a pre save method to hash the password
 */
PlayerSchema.pre('save', function (next) {
    if (this.password && this.isModified('password')) {
        this.salt = crypto.randomBytes(16).toString('base64');
        this.password = this.hashPassword(this.password);
    }

    next();
});

/**
 * Hook a pre validate method to test the local password
 */
PlayerSchema.pre('validate', function (next) {
    if (this.provider === 'local' && this.password && this.isModified('password')) {
        let result = owasp.test(this.password);
        if (result.errors.length) {
            let error = result.errors.join(' ');
            this.invalidate('password', error);
        }
    }

    next();
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
PlayerSchema.methods = {
    /**
    * Create instance method for authenticating player
    * @param {password}
    */
    authenticate(password) {
        return this.password === this.hashPassword(password);
    },

    /**
    * Create instance method for hashing a password
    * @param {password}
    */
    hashPassword(password) {
        if (this.salt && password) {
            return crypto.pbkdf2Sync(password, new Buffer(this.salt, 'base64'), 10000, 64, 'SHA1').toString('base64');
        } else {
            return password;
        }
    }
};


PlayerSchema.statics = {

    /**
     * save and update Player
     * @param Player
     * @returns {Promise<Player, APIError>}
     */
    save(player) {
        return player.save()
            .then((player) => {
                if (player) {
                    return player;
                }
                let req = {};
                req.errorKey = 'PlayerCreateError';
                errorService.insertActivity(req);
                const err = new APIError('error in player', httpStatus.NOT_FOUND);
                return Promise.reject(err);
            });

    },

    /**
   * List player in descending order of 'createdAt' timestamp.
   * @returns {Promise<player[]>}
   */
    list(query) {
        return this.find(query.filter)
            .sort(query.sorting)
            .skip((query.page - 1) * query.limit)
            .limit(query.limit)
            .exec();
    },

    /**
     * Count of player records
     * @returns {Promise<player[]>}
     */
    totalCount(query) {
        return this.find(query.filter)
            .countDocuments();
    },
    /**
     * Get player
     * @param {ObjectId} id - The objectId of player.
     * @returns {Promise<player, APIError>}
     */
    get(id) {
        return this.findById(id)
            .exec()
            .then((player) => {
                if (player) {
                    return player;
                }
                let req = {};
                req.errorKey = 'NoSuchPlayerExist';
                errorService.insertActivity(req);
                const err = new APIError('No such player exists', httpStatus.NOT_FOUND);
                return Promise.reject(err);
            })
    },

    /**
     * Find unique email.
     * @param {string} email.
     * @returns {Promise<Player[]>}
     */
    findUniqueEmail(email) {
        email = email.toLowerCase();
        return this.findOne({
            email: email,
            active: true
        })
            .exec()
            .then((player) => player);
    }
}

export default mongoose.model('Player', PlayerSchema);
