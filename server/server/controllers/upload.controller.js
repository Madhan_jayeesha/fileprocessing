import fs from 'fs';
var XLSX = require('xlsx');

import config from '../config/config';

import Player from '../models/player.model';

import respUtil from '../utils/resp.util';
import i18nUtil from '../utils/i18n.util';

import uploadeService from '../services/upload.service';
import PlayerService from '../services/player.service';

const controller = "Upload";
const playerService = new PlayerService();

/** 
 * Upload pictures and documents
 */

/**
 *quantities bulk upload.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function bulkUpload(req, res) {
	console.log('####')
	logger.info('Log:user Controller:uploadCsvFile: Query : ' + JSON.stringify(req.query));
	req.uploadFile = [];
	req.uploadPath = 'xlsx';
	uploadeService.upload(req, res, async (err) => {
		if (err) {
			res.json({ 'errorCode': config.statusCodes.ERROR, 'errorMessage': err });
			return;
		} else {
			var workbook = XLSX.readFile(config.upload[req.uploadPath] + "/" + req.uploadFile[0].name, { cellDates: true });
			var sheet_name_list = workbook.SheetNames;
			let rowStart;
			for (let y of sheet_name_list) {
				var worksheet = workbook.Sheets[y];
				var userData = [];
				var headers = {};
				for (var z in worksheet) {
					if (z[0] === '!')
						continue;
					// parse out the column, row, and value
					var tt = 0;
					for (var i = 0; i < z.length; i++) {
						if (!isNaN(z[i])) {
							headers[i] = '';
							tt = i;
							break;
						}
					}
					var col = z.substring(0, tt);
					var row = parseInt(z.substring(tt), 10);
					var value = worksheet[z].w;
					// console.log(row, ' ', col, ' ', z, ' ', worksheet[z], '####')
					//for date object value
					if (worksheet[z].t === 'd') {
						var value = worksheet[z].v;
					}
					if (value === 'RANK') {
						rowStart = row;
					}
					// console.log(row + ' ' + col, '@@@@@@@');
					// store header names
					let columns = 'FGHIJKLMNOPQRSTUVWXYZ';
					if (row == rowStart) {
						// console.log(col + (row - 3));
						var key = '';
						for (let i = row - 3; i < row && (columns.includes(col)); i++) {
							// console.log(col + i, ' ', worksheet[col + i], '$$$$$')
							if (worksheet[col + i] && worksheet[col + i].t !== 'n' && worksheet[col + i].t !== 'd') {
								key = worksheet[col + i].v ? key + worksheet[col + i].v : key;
							}
						}
						key = key + value;
						key = key.replace(/\s/g, '');
						if (key.includes("25%PTS.AsianU-14")) {
							key = '25%PTS.AsianU-14';
						}
						// var key = worksheet[col + (row - 3)].v + worksheet[col + (row - 2)].v + worksheet[col + (row - 1)].v + worksheet[col + row].v
						headers[col] = key.toUpperCase();
					}
					if (!userData[row])
						userData[row] = {};
					userData[row][headers[col]] = value;
				}
				req.eventDate = worksheet['B' + (rowStart - 2)] ? worksheet['B' + (rowStart - 2)].v : '';
				req.category = worksheet['B' + (rowStart - 3)] ? worksheet['B' + (rowStart - 3)].v : '';

				// drop those unwanted rows 
				for (let i = 0; i <= rowStart; i++) {
					userData.shift();
				}
				req.obj = userData;
				await playerService.insertUserData(req, res);

			}
			// if (req.duplicates && req.duplicates.length > 0) {
			// 	// console.log(req.duplicates, '####')
			// 	req.entityType = 'playerRecord'
			// 	await downloadDupicates(req);
			// 	req.i18nKey = "duplicateUpload";
			// 	res.json(respUtil.getErrorResponse(req));
			// } else {
			req.i18nKey = 'fileUploadSuccess';
			logger.info('Log:Upload Controller:BulkUpload:' + i18nUtil.getI18nMessage(req.i18nKey), controller);
			res.json(respUtil.successResponse(req));
			// }
		}
	});
};

/**
 * 
 * @return {Downloaded fie} req 
 */
async function downloadDupicates(req, res) {
	let ws = XLSX.utils.json_to_sheet(req.duplicates);
	let wb = XLSX.utils.book_new();
	XLSX.utils.book_append_sheet(wb, ws);
	req.duplicateFileName = req.entityType + "duplicates" + Date.now() + "_" + ".xlsx"
	let buf = XLSX.writeFile(wb, config.upload[`duplicate`] + '/' + req.duplicateFileName);
};





export default {
	bulkUpload,
}