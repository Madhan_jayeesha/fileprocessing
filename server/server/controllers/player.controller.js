import moment from 'moment';

import Player from '../models/player.model';

// import activityService from '../services/activity.service';
import PlayerService from '../services/player.service';

import i18nUtil from '../utils/i18n.util';
import respUtil from '../utils/resp.util';
import serviceUtil from '../utils/service.util';
import sessionUtil from '../utils/session.util';


const playerService = new PlayerService();
const controller = "Player";

/**
 * Load Player and append to req.
 * @param req
 * @param res
 * @param next
 */

async function load(req, res, next) {
    req.player = await Player.get(req.params.playerId);
    return next();
}

/**
 * Get Player
 * @param req
 * @param res
 * @returns {details: player}
 */

async function get(req, res) {
    logger.info('Log:Player Controller:get: query :' + JSON.stringify(req.query), controller);

    await serviceUtil.checkPermission(req, res, "View", controller);
    req.query = await serviceUtil.generateListQuery(req);
    let player = req.player;
    logger.info('Log:player Controller:get:' + i18nUtil.getI18nMessage('recordFound'), controller);
    let responseJson = {
        respCode: respUtil.getDetailsSuccessResponse().respCode,
        details: player
    };
    return res.json(responseJson);
}

/**
 * Create new player
 * @param req
 * @param res
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function create(req, res) {
    logger.info('Log:Player Controller:create: body :' + JSON.stringify(req.body), controller);

    await serviceUtil.checkPermission(req, res, "Edit", controller);
    let player = new Player(req.body);

    player = await playerService.setCreatePlayerVariables(req, player)
    req.player = await Player.save(player);
    req.player.password = req.player.salt = undefined;
    req.entityType = 'player';
    req.activityKey = 'playerCreate';
    req.enEmail = serviceUtil.encodeString(req.player.email);
    // activityService.insertActivity(req);
    logger.info('Log:player Controller:create:' + i18nUtil.getI18nMessage('playerCreate'), controller);
    return res.json(respUtil.createSuccessResponse(req));
}

/**
* Get player list. based on criteria
* @param req
* @param res
* @param next
* @returns {players: players, pagination: pagination}
*/
async function list(req, res, next) {
    let responseJson = {};
    logger.info('log:Player Controller:list:query :' + JSON.stringify(req.query), controller);

    await serviceUtil.checkPermission(req, res, "View", controller);
    const query = await serviceUtil.generateListQuery(req);
    //get player records
    if (query.page === 1)
        // total count
        query.pagination.totalCount = await Player.totalCount(query);

    const players = await Player.list(query);
    logger.info('Log:player Controller:list:' + i18nUtil.getI18nMessage('recordsFound'), controller);
    responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
    responseJson.players = players;
    responseJson.pagination = query.pagination;
    return res.json(responseJson);
}


/**
 * Update existing player
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function update(req, res, next) {
    req.query = await serviceUtil.generateListQuery(req);
    logger.info('Log:Player Controller:update: body :' + JSON.stringify(req.body), controller);

    await serviceUtil.checkPermission(req, res, "Edit", controller);
    let player = req.player;

    player = Object.assign(player, req.body);
    player = await playerService.setUpdatePlayerVariables(req, player)
    req.player = await Player.save(player);
    req.entityType = 'player';
    req.activityKey = 'playerUpdate';
    // activityService.insertActivity(req);
    logger.info('Log:player Controller:update:' + i18nUtil.getI18nMessage('playerUpdate'), controller);
    return res.json(respUtil.updateSuccessResponse(req));
}

/**
 * Delete player.
 * @param req
 * @param res
 * @param next
 * @returns { respCode: respCode, respMessage: respMessage }
 */
async function remove(req, res, next) {
    logger.info('Log:Player Controller:remove: query :' + JSON.stringify(req.query), controller);

    await serviceUtil.checkPermission(req, res, "Edit", controller);
    let player = req.player;
    player = await playerService.setUpdatePlayerVariables(req, player)
    player.active = false;
    req.player = await Player.save(player);
    req.entityType = 'player';
    req.activityKey = 'playerDelete';
    // activityService.insertActivity(req);
    logger.info('Log:player Controller:remove:' + i18nUtil.getI18nMessage('playerDelete'), controller);
    res.json(respUtil.removeSuccessResponse(req));
}

async function getPlayerData(req, res) {
    let responseJson = {};
    logger.info('log:Player Controller:list:query :' + JSON.stringify(req.query), controller);

    await serviceUtil.checkPermission(req, res, "View", controller);
    const query = await serviceUtil.generateListQuery(req);
    //get player records

    if (req.query.displayName)
        query.filter.displayName = req.query.displayName;

    const players = await Player.aggregate([
        { $match: query.filter },
        { $sort: { eventDate: -1 } },
        { $group: { "_id": "$category", data: { $push: '$$ROOT' } } },
    ])

    logger.info('Log:player Controller:list:' + i18nUtil.getI18nMessage('recordsFound'), controller);
    responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
    responseJson.players = players;
    return res.json(responseJson);
}

async function getPlayerNames(req, res) {
    let responseJson = {}, uniquePlayerNames = [];
    logger.info('log:Player Controller:list:query :' + JSON.stringify(req.query), controller);

    await serviceUtil.checkPermission(req, res, "View", controller);
    const query = await serviceUtil.generateListQuery(req);
    query.limit = null;
    //get player records
    const players = await Player.list(query);
    const playerNames = [...new Set(players.map((item) => item.displayName))];
    playerNames.forEach(item => { uniquePlayerNames.push({ displayName: item }) });

    logger.info('Log:player Controller:getPlayerNames:' + i18nUtil.getI18nMessage('recordsFound'), controller);
    responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
    responseJson.players = uniquePlayerNames;
    responseJson.pagination = uniquePlayerNames.length;
    return res.json(responseJson);
}

async function playerGraphData(req, res) {
    let responseJson = {}, graphData = [];
    logger.info('log:Player Controller:list:query :' + JSON.stringify(req.query), controller);

    await serviceUtil.checkPermission(req, res, "View", controller);
    const query = await serviceUtil.generateListQuery(req);

    if (req.query.displayName)
        query.filter.displayName = req.query.displayName;
    if (req.query.category)
        query.filter.category = req.query.category;

    query.sorting = { 'eventDate': 1 };
    query.limit = null;
    //get player records
    const players = await Player.list(query);
    players.forEach(item => {
        let data = {
            displayName: item.displayName,
            eventDate: moment(item.eventDate).format('YYYY/MM/DD'),
            rank: item.rank,
            ttlFinalPoints: parseFloat(item.ttlFinalPoints)
        };
        graphData.push(data);
    });
    logger.info('Log:player Controller:list:' + i18nUtil.getI18nMessage('recordsFound'), controller);
    responseJson.respCode = respUtil.getDetailsSuccessResponse().respCode;
    responseJson.graphData = graphData;
    return res.json(responseJson);
}


export default {
    create,
    list,
    get,
    load,
    update,
    remove,
    getPlayerData,
    getPlayerNames,
    playerGraphData
};
