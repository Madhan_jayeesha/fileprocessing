import moment from 'moment';
import Player from '../models/player.model';

class PlayerService {
    constructor() {

    }

    async insertUserData(req, res) {
        req.duplicates = [];
        let obj = req.obj;
        // console.log(obj, '#####');
        for (let val of obj) {
            let player = new Player();
            // console.log(val)
            if (val) {
                if (val['RANK']) {
                    player.rank = val['RANK'];
                }
                if (val['GIVENNAME']) {
                    player.firstName = val['GIVENNAME'];
                }
                if (val['FAMILYNAME']) {
                    player.lastName = val['FAMILYNAME'];
                }
                if (val['NAMEOFPLAYER']) {
                    let displayName = val['NAMEOFPLAYER'];
                    displayName = displayName.replace(/\s+/g, ' ');
                    player.displayName = displayName;
                }
                if (val['REGNO.']) {
                    player.regNumber = val['REGNO.'];
                }
                if (val['DOB'] || val['D.O.B']) {
                    player.dob = val['DOB'] ? val['DOB'] : val['D.O.B'];
                    let x = new moment();
                    let y = new moment(player.dob);
                    let age = moment.duration(x.diff(y))
                    if (age && age._data)
                        player.age = age._data.years;
                }
                if (val['STATE']) {
                    player.state = val['STATE'];
                }
                if (val['BESTEIGHTSING.PTS.'] || val['BESTEightSing.PTS.']) {
                    player.bestEightSinglePoints = val['BESTEIGHTSING.PTS.']
                    player.bestEightCategory = true;
                }
                if (val['BESTEIGHTDBLS.PTS.'] || val['BESTEightDbls.PTS.']) {
                    player.bestEightDoublePoints = val['BESTEIGHTDBLS.PTS.']
                }
                if (val['25%BESTEIGHTDBLS.PTS.'] || val['25%BESTEightDbls.PTS.']) {
                    player.twentyFivePercentBestEightDoublePoints = val['25%BESTEIGHTDBLS.PTS.'];
                }
                if (val['BESTSIXSING.PTS.'] || val['BESTSIXSing.PTS.']) {
                    player.bestSixSinglePoints = val['BESTSIXSING.PTS.'];
                    player.bestSixCategory = true;
                }
                if (val['BESTSIXDBLS.PTS.'] || val['BESTSIXDbls.PTS.']) {
                    player.bestSixDoublePoints = val['BESTSIXDBLS.PTS.'];
                }
                if (val['25%BESTSIXDBLS.PTS.'] || val['25%BESTSIXDbls.PTS.']) {
                    player.twentyFivePercentBestSixSinglePoints = val['25%BESTSIXDBLS.PTS.'];
                }
                if (val['25%PTS.ASIANU-14']) {
                    player.twentyFivePercentPointsAsianU14 = val['25%PTS.ASIANU-14']
                }
                if (val['PTS.UNDER14']) {
                    player.pointsUnder14 = val['PTS.UNDER14']
                }
                if (val['PTS.UNDER16']) {
                    player.pointsUnder16 = val['PTS.UNDER16']
                }
                if (val['PTS.UNDER18']) {
                    player.pointsUnder18 = val['PTS.UNDER18']
                }
                if (val['PTS.UNDERMENS']) {
                    player.pointsUnderMens = val['PTS.UNDERMENS'];
                }
                if (val["PTS.UNDERWOMEN\'S"]) {
                    player.pointsUnderWomens = val["PTS.UNDERWOMEN\'S"];
                }
                if (val['ITFPTS.X2']) {
                    player.itfPoints = val['ITFPTS.X2'];
                }
                if (val['POINTSCUTFORNOSHOWLATEWL']) {
                    player.pointsCutForNoShowLateWL = val['POINTSCUTFORNOSHOWLATEWL'];
                }
                if (val['TTL.PTS.FINAL'] || val['TTL.PTS.']) {
                    player.ttlFinalPoints = val['TTL.PTS.FINAL'] ? val['TTL.PTS.FINAL'] : val['TTL.PTS.'];
                }
                if (req.eventDate) {
                    let eventDate = req.eventDate;
                    eventDate = eventDate.replace(',', '');
                    eventDate = eventDate.replace('Year End', '31 Dec');
                    eventDate = eventDate.replace(/(th|st|nd|rd)/g, '');
                    if (eventDate) {
                        eventDate = new Date(eventDate + 'Z');
                        player.eventDate = eventDate;
                    }
                }
                if (req.category) {
                    player.category = req.category;
                    player.category = player.category.replace(/\s+/g, ' ');
                }
                if (!player.displayName)
                    player = await this.setCreatePlayerVariables(req, player);


                // if (player) {
                //     let uniqueRecord = await Player.findOne({ category: player.category, eventDate: player.eventDate, regNumber: player.regNumber });
                //     //if record exists the particule record will added to the duplicate object
                //     if (uniqueRecord) {
                //         val.reason = "Player record alredy existed";
                //         req.duplicates.push(val);
                //     } else {
                req.player = await Player.save(player);
                req.entityType = 'player';
                req.activityKey = 'playerCreate';
                //     }
                // }
            }
        }
    }

    async setCreatePlayerVariables(req, player) {
        player.displayName = "";
        if (player.firstName) {
            player.displayName += player.firstName + " ";
        }
        if (player.lastName) {
            player.displayName += player.lastName
        }
        return player;
    }
}

export default PlayerService