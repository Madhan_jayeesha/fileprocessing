import multer from 'multer';
import moment from 'moment';
import fs from 'fs';
import XLSX from 'xlsx';

import config from '../config/config';

import playerService from '../services/player.service';

import respUtil from '../utils/resp.util';


/**
 * Storing Uploades file
 * @return {uploded file name}
 */
let storage = multer.diskStorage({
    destination: function (req, file, callback) {
        if (!fs.existsSync(config.upload[req.uploadPath])) {
            console.log(req.uploadPath, '....')
            console.log(config.upload[req.uploadPath])
            fs.mkdirSync(config.upload[req.uploadPath])
        }
        callback(null, config.upload[req.uploadPath]);
    },
    filename: function (req, file, callback) {
        let ext = '';
        let name = '';
        if (file.originalname) {
            let p = file.originalname.lastIndexOf('.');
            ext = file.originalname.substring(p + 1);
            let firstName = file.originalname.substring(0, p + 1);
            name = Date.now() + '_' + firstName;
            name += ext;
        }
        req.uploadFile.push({ name: name, uploadedDate: moment().format('DD/MM/YYYY') });
        if (req.uploadFile && req.uploadFile.length > 0) {
            callback(null, name);
        }
    }
});

const upload = multer({
    storage: storage
}).array('file');


async function getJsonFromCsv(req) {
    var obj = [];
    console.log("upppppppppploooooooooooooaaaaaaaaa")
    console.log(req.uploadFile[0].name)
    req.attachment = req.uploadFile[0].name;
    let workbook = XLSX.readFile(config.upload[req.uploadPath] + "/" + req.uploadFile[0].name);
    if (workbook) {
        let sheetName_list = workbook.SheetNames;
        obj = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName_list[0]]);
    }
    // console.log(obj)
    return obj;

};

/**
 * 
 * @return {Downloaded fie} req 
 */
async function downloadDupicates(req, res) {
    let ws = XLSX.utils.json_to_sheet(req.duplicates);
    let wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws);
    req.duplicateFileName = req.entityType + "duplicates" + Date.now() + "_" + ".xlsx"
    let buf = XLSX.writeFile(wb, config.upload[`duplicates`] + '/' + req.duplicateFileName);
};


async function insertBulkData(req, res) {
    let responseJson = {};
    let obj;
    if (req.query && req.query.type) {
        if (req.query.type === "player") {
            obj = await playerService.insertUserData(req, res);
        }
        else if (req.query.type === 'uniqueNumber') {
            req.entityType = 'uniqueNumber';
            obj = await uniqueNumberService.insertUniqueNumberData(req, res);
        }
        else {
            req.i18nKey = "failedtoUpload";
            res.json(respUtil.getErrorResponse(req));
            return;
        }
        // if dupicate email exists in the file a new file will get created with duplicates
        if (req.duplicates && req.duplicates.length > 0) {
            await downloadDupicates(req);

            req.i18nKey = "duplicateUpload";
            responseJson.failure = respUtil.getErrorResponse(req);
        };
        obj = obj.filter(function (item) { return item != null })
        if (obj && Object.keys(obj).length == 0) {
            responseJson.failure.message = "All Numbers are duplicates";
        } else {
            req.entityType = req.query.type + "Csv";
            req.activityKey = `${req.query.type}CsvUpload`;
            responseJson.sucess = respUtil.getDetailsSuccessResponse(req)
            // };
        };
        return responseJson;
    }
}

export default {
    upload,
    insertBulkData,
    downloadDupicates,
    getJsonFromCsv
}