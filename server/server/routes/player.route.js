import express from 'express';
import validate from 'express-validation';
import paramValidate from '../config/param-validation';
import playerCtrl from '../controllers/player.controller';
import asyncHandler from 'express-async-handler';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
    /** POST /api/players - Create new players */
    .post(validate(paramValidate.createEmployee), asyncHandler(playerCtrl.create))

    /** get /api/players -  get all players */
    .get(asyncHandler(playerCtrl.list));

router.route('/playerData')
    .get(asyncHandler(playerCtrl.getPlayerData))

router.route('/playerNames')
    .get(asyncHandler(playerCtrl.getPlayerNames))

router.route('/playerGraphData')
    .get(validate(paramValidate.playerGraphData), asyncHandler(playerCtrl.playerGraphData));

router.route('/:playerId')
    /** get /api/players -  get one players using id*/
    .get(asyncHandler(playerCtrl.get))

    /** put /api/players -  update players */
    .put(validate(paramValidate.updateEmployee), asyncHandler(playerCtrl.update))

    /** delete /api/players -  delete players */
    .delete(asyncHandler(playerCtrl.remove));

router.param('playerId', asyncHandler(playerCtrl.load));

export default router;