import express from 'express';

import uploadRoutes from './upload.route';

import playerRoutes from './player.route';

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */

//Mount upload routes at /uploads
router.use('/uploads', uploadRoutes);

//Mount upload routes at /uploads
router.use('/players', playerRoutes);


export default router;
