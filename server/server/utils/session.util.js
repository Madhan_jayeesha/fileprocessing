
//Check the existance of tokenInfo and tokenInfo Fields
const checkTokenInfo = (req, filed) => {
  if (!filed)
    return req && req.tokenInfo ? true : false;
  else
    return req && req.tokenInfo && req.tokenInfo[filed] ? true : false;
};

//Get the value of tokenInfo and tokenInfo Fields
const getTokenInfo = (req, filed) => {
  if (!filed)
    return req && req.tokenInfo ? req.tokenInfo : {};
  else
    return req && req.tokenInfo && req.tokenInfo[filed] ? req.tokenInfo[filed] : '';
};

export default {
  checkTokenInfo,
  getTokenInfo
}